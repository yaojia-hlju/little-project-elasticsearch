package com.ruyuan.little.project.elasticsearch.biz.admin.service;

import com.ruyuan.little.project.common.dto.CommonResponse;
import com.ruyuan.little.project.common.dto.TableData;
import com.ruyuan.little.project.elasticsearch.biz.admin.dto.AdminGoodsStoreDTO;
import com.ruyuan.little.project.elasticsearch.biz.admin.entity.AdminGoodsStore;

/**
 * @author dulante
 * version: 1.0
 * Description:后台商品店铺service组件
 **/
public interface AdminGoodsStoreService {

    /**
     * 根据店铺名称获取店铺列表 从MySQL查
     *
     * @param adminGoodsStoreDTO 店铺分页查询信息
     * @return 结果
     */
    CommonResponse<TableData<AdminGoodsStore>> getStorePageByStoreNameFromDB(AdminGoodsStoreDTO adminGoodsStoreDTO);

    /**
     * 根据店铺名称获取店铺列表 从es查询
     *
     * @param adminGoodsStoreDTO 店铺分页查询信息
     * @return 结果
     */
    CommonResponse<TableData<AdminGoodsStore>> getStorePageByStoreNameFromEs(AdminGoodsStoreDTO adminGoodsStoreDTO);


    /**
     * 添加店铺
     *
     * @param adminGoodsStore 店铺信息
     * @return 结果
     */
    CommonResponse insertGoodsStore(AdminGoodsStore adminGoodsStore);



    /**
     * 根据id删除商品
     *
     * @param id id
     * @return 结果
     */
    CommonResponse deleteStoreById(String id);
}
